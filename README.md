## PGIA7326

<a href="http://ufrpe.br/" target="_blank">Federal Rural University of Pernambuco</a><br/>
<a href="http://www.ppgia.ufrpe.br/" target="_blank">Master's degree programme in Applied Informatics</a><br/>
<a href="http://172.16.128.2/~tiago/TS/ts.htm" target="_blank">PGIA7326 Course - Time Series - 2018.2</a><br/>

## Support

Reach out to me at one of the following places!

- Website at <a href="http://ddefb.me/" target="_blank">`ddefb.me`</a>
- Twitter at <a href="https://twitter.com/DB556IA2" target="_blank">`@DB556IA2`</a>


## License

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

- **[MIT license](http://opensource.org/licenses/mit-license.php)**
- Copyright 2018 © <a href="http://ddefb.me/" target="_blank">Diego Bezerra</a>.
