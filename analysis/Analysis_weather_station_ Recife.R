library(forecast)
library(ggplot2)

# import data csv 
dataBrasiliaWeatherStation = read.csv(file.choose(), sep = ",")

dataTempMax = dataBrasiliaWeatherStation[c(1:2160),c(9)]

#dataTempMax = dataRecifeWeatherStation[,c(9)]

#creation monthly time series of the year 2017 

dataBrasiliaWeatherStation = ts (data = dataTempMax, start =c(1, 2017) , end = c(3, 2017) , frequency = 24 * 30)

#plot using ggplot2
autoplot(dataBrasiliaWeatherStation)

prev = auto.arima(dataBrasiliaWeatherStation)
