

summary_test <- function(series, m0=0, equal.variance=FALSE) {
  # print(series)
  values = series[,ncol(series)]
  split = length(values) / 2
  X1 = head(values, n=split)
  X2 = tail(values, n=split)
  
  m1 = mean(X1)
  m2 = mean(X2)
  s1 = sd(X1)
  s2 = sd(X2)
  n1 = length(X1)
  n2 = length(X2)
  
  if( equal.variance==FALSE ) {
    se <- sqrt( (s1^2/n1) + (s2^2/n2) )
    df <- ( (s1^2/n1 + s2^2/n2)^2 )/( (s1^2/n1)^2/(n1-1) + (s2^2/n2)^2/(n2-1) )
  } else {
    se <- sqrt( (1/n1 + 1/n2) * ((n1-1)*s1^2 + (n2-1)*s2^2)/(n1+n2-2) ) 
    df <- n1+n2-2
  }
  
  t <- (m1-m2-m0)/se 
  dat <- c(m1-m2, se, t, 2*pt(-abs(t),df))    
  names(dat) <- c("Difference of means", "Std error", "t", "p-value")
  print(dat)
  
  if(dat[4] > 0.05) {
    return(TRUE)
  } else {
    return(FALSE)
  }
}

# summary_test = function(series) {
#   values = series[,ncol(series)]
#   split = length(values) / 2
#   result = t.test(head(values, n=split), 
#                   tail(values, n=split), 
#                   conf.level=.95, 
#                   var.equal = TRUE)
#   
#   if(result$p.value > 0.05) {
#     return(TRUE)
#   }else {
#     return(FALSE)
#   }
# }