
source("./R/StationarityTest.R")
source("./R/Utils.R")
source("./R/Metrics.R")
library(forecast)
library(ggplot2)

evaluate_arima_model <- function(series, order = c(0,0,0)) {
  values = series[,ncol(series)]
  size = as.integer((length(values) * 0.7))
  train = head(values, n=size)
  test = tail(values, n=(length(values)-size))
  predictions = NULL
  history = data.frame(c(numeric(0)))
  for(i in train) {
    history = rbind(history, i)
  }
  
  yhat = NULL
  
  for(t in test){
    model = arima(history[,ncol(history)], order)
    
    yhat = forecast(model, h=1)
    # a = acf(yhat$residuals, lag.max=20, ci=0)
    predictions = rbind(predictions, data.frame(yhat$mean[1]))
    history = rbind(history, t)
  }
  # hist(yhat$residuals)
  
  #Comentar com mestre diego esta validacao
  # if (!shapiro.test(yhat$residuals)) {
  #   return(Inf)
  # }else {
  #   return(mean_squared_error(test, predictions[,ncol(predictions)]))
  # }
  return(mean_squared_error(test, predictions[,ncol(predictions)]))
}

#' @title Select best Arima model to univariate time series
#' @name evaluate_models
#'
#' @description Returns best ARIMA model according with MSE value
#' 
#' @param series a time series dataset.
#' @param p_max = maximum value to autoregressive term
#' @param d_max = maximum value to order of differencing  
#' @param q_max = Maximum value to moving average term
#' 
#' @details Parameter 'values' need to be a 'ts object'.
#' Parameter 'p_max' has the default value 5
#' Parameter 'd_max' has the default value 2
#' Parameter 'q_max' has the default value 5
#' has the default value 1
#'
#' @return Returns the best Arima model selected and the MSE value
#'
#' @examples
#' evaluate_models(salesShampoo, p_max=5, d_max=2, q_max=5)
#'
#'@import forecast
#'@import ggplot2
#' @export

evaluate_models = function(series, p_max=5, d_max=2, q_max=5) {
  best_score = Inf
  best_cfg = 'ARIMA'
  
  if(!is_stationary(series)){
    d_max = ndifferences(series)
  }else{d_max = 0}
  
  for(p in 0:p_max) {
    for(q in 0:q_max) {
      result = tryCatch({
        order = c(p, d_max, q)
        
        mse = evaluate_arima_model(series, order)
        
        if(mse < best_score){
          best_score = mse
          best_cfg = order
        }
        
        print(sprintf("ARIMA(%1.f, %1.f, %1.f) - MSE: %1.2f", order[1], order[2], order[3], mse))
        
      }, warning = function(warning_condition) {}, error = function(error_condition) {}, finally={})
    }
  }
  
  print(sprintf("Best model: ARIMA(%1.f, %1.f, %1.f) - MSE: %1.2f", best_cfg[1], best_cfg[2], best_cfg[3], best_score))
}
